﻿using DataAccess.Contracts;
using DataAccess.Entities;
using DataAccess.Repositories;
using Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class CategoryModel
    {
        private int id;
        private string code;
        private string name;
        private string description;

        private EntityState state;
        private ICategoryRepository categoryRepository;

        public CategoryModel()
        {
            categoryRepository = new CategoryRepository();
        }

        private int Id { get => id; set => id = value; }
        public string Code { get => code; set => code = value; }
        public string Name { get => name; set => name = value; }
        public string Description { get => description; set => description = value; }
        public EntityState State { private get => state; set => state = value; }

        public int SaveChanges()
        {
            int result = 0;

            Category category = new Category
            {
                Id = id,
                Code = code,
                Name  = name,
                Description = description
            };

            switch(state)
            {
                case EntityState.Added:
                    result = categoryRepository.Add(category);
                    break;
                case EntityState.Modified:
                    result = categoryRepository.Edit(category);
                    break;
                case EntityState.Deleted:
                    result = categoryRepository.Delete(id);
                    break;
            }

            return result;
        }

        public List<CategoryModel> GetAll()
        {
            List<CategoryModel> categories = new List<CategoryModel>();
            foreach(Category category in categoryRepository.GetAll())
            {
                categories.Add(new CategoryModel
                {
                    id = category.Id,
                    code = category.Code,
                    name = category.Name,
                    description = category.Description
                });
            }

            return categories;
        }
    }
}
