﻿using DataAccess.Contracts;
using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class CategoryRepository : MasterRepository, ICategoryRepository
    {
        private string select;
        private string update;
        private string insert;
        private string delete;

        public CategoryRepository()
        {
            this.select = "GetAllCategory";
            this.update = "UpdateCategory";
            this.insert = "InsertCategory";
            this.delete = "DeleteCategory";
        }

        public int Add(Category e)
        {
            parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@name", e.Name));
            parameters.Add(new SqlParameter("@description", e.Description));
            return ExecuteNonQuery(insert);
        }

        public int Delete(int id)
        {
            parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@id", id));
            return ExecuteNonQuery(delete);
        }

        public int Edit(Category e)
        {
            parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@id", e.Id));
            parameters.Add(new SqlParameter("@name", e.Name));
            parameters.Add(new SqlParameter("@description", e.Description));
            return ExecuteNonQuery(update);
        }

        public IEnumerable<Category> GetAll()
        {
            parameters = new List<SqlParameter>();
            List<Category> categories = new List<Category>();
            foreach(DataRow categoryRow in ExecuteReader(select).Rows)
            {
                categories.Add(new Category
                {
                    Id = int.Parse(categoryRow["Id"].ToString()),
                    Code = categoryRow["Code"].ToString(),
                    Name = categoryRow["Name"].ToString(),
                    Description = categoryRow["Description"].ToString()
                });
            }

            return categories;
        }
    }
}
