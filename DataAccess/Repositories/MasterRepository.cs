﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public abstract class MasterRepository : Repository
    {
        protected List<SqlParameter> parameters;

        protected int ExecuteNonQuery(string transactSql)
        {
            using(var connection = GetConnection())
            {
                connection.Open();
                using(var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = transactSql;
                    command.CommandType = CommandType.StoredProcedure;
                    foreach(SqlParameter param in parameters)
                    {
                        command.Parameters.Add(param);
                    }
                    int result = command.ExecuteNonQuery();
                    parameters.Clear();
                    return result;
                }
            }
        }

        protected DataTable ExecuteReader(string transactSql)
        {
            using(var connection = GetConnection())
            {
                connection.Open();
                using(var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = transactSql;
                    command.CommandType = CommandType.Text;
                    foreach(SqlParameter param in parameters)
                    {
                        command.Parameters.Add(param);
                    }
                    DataTable table = new DataTable();
                    using(var reader = command.ExecuteReader())
                    {
                        parameters.Clear();
                        table.Load(reader);
                        return table;
                    }
                }
            }
        }
    }
}
