﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public abstract class Repository
    {
        protected readonly string connectionMantenimientoProductos;

        public Repository()
        {
            connectionMantenimientoProductos = ConfigurationManager.ConnectionStrings["Presentation.Properties.Settings.connMantenimientoProductos"].ConnectionString;
        }

        protected SqlConnection GetConnection()
        {
            return new SqlConnection(connectionMantenimientoProductos);
        }
    }
}
