﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Contracts
{
    public interface IRepository<Entity> where Entity : class
    {
        int Add(Entity e);
        int Edit(Entity e);
        int Delete(int id);
        IEnumerable<Entity> GetAll();
    }
}
