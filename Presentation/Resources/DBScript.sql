create database MantenimientoProductos
go

use MantenimientoProductos
go

create table Category (
	id int identity(1, 1) primary key,
	code as ('CT'+right('00' + convert(varchar, id), (2))),
	name varchar(30) not null,
	description varchar(255)
);
go

--create table Mark (
	
--);
--go

--create table Product (
	
--);
--go

-- Obtener Todo en Cataegoria
create procedure GetAllCategory
as
	select * from Category;
go

-- Buscar Categoria
create procedure FindCategory (
	@name varchar(30)
)
as
	select * from Category where name like @name+'%';
go

-- Insertar Categoria
create procedure InsertCategory (
	@name varchar(30),
	@description varchar(255)
)
as
	insert into Category values (@name, @description);
go

-- Editar Categoria
create procedure UpdateCategory (
	@id int,
	@name varchar(30),
	@description varchar(255)
)
as
	update Category set name = @name, description = @description where id = @id;
go

-- Eliminar Categoria
create procedure DeleteCategory(
	@id int
)
as
	delete from Category where id = @id;
go
