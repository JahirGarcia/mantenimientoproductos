﻿using Domain.Models;
using Domain.ValueObjects;
using Equin.ApplicationFramework;
using Presentation.Helps;
using Presentation.ViewContracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentation.Presenters
{
    class CategoryPresenter
    {
        private IViewCategory categoryView;
        private CategoryModel categoryModel;
        public CategoryPresenter(IViewCategory categoryView)
        {
            this.categoryView = categoryView;
            categoryModel = new CategoryModel();
        }

        public void ListCategory()
        {
            List<CategoryModel> categories = categoryModel.GetAll();
            BindingListView<CategoryModel> blvCategory = new BindingListView<CategoryModel>(categories);

            BindingSource bsCategories = categoryView.BsCategories;
            DataGridView dgvCategories = categoryView.DgvCategories;

            bsCategories.DataSource = blvCategory;

            dgvCategories.DataSource = bsCategories;
            dgvCategories.AutoGenerateColumns = true;
        }

        public void FilterTable()
        {
            BindingListView<CategoryModel> blvCategory = (BindingListView<CategoryModel>)categoryView.BsCategories.DataSource;
            string filterText = categoryView.FilterText;
            blvCategory.ApplyFilter(c => c.Code.ToLower().Contains(filterText.ToLower()) || c.Name.ToLower().Contains(filterText.ToLower()));
        }

        public void Save()
        {
            BindingSource bsCategories = categoryView.BsCategories;
            CategoryModel savedCategory = ((ObjectView<CategoryModel>)bsCategories.CurrencyManager.Current).Object;

            savedCategory.State = (categoryView.SavedMode == SavedMode.Add) ? EntityState.Added : EntityState.Modified;
            savedCategory.Code = categoryView.CategoryCode;
            savedCategory.Name = categoryView.CategoryName;
            savedCategory.Description = categoryView.CategoryDescription;

            savedCategory.SaveChanges();
            bsCategories.EndEdit();
        }

        public void Remove()
        {
            BindingSource bsCategories = categoryView.BsCategories;

            if(bsCategories.Count < 1)
            {
                categoryView.ShowInfoMessage("No hay elementos que eliminar");
                return;
            }

            DialogResult result = categoryView.ShowQuestionMessage("Seguro que desea eliminar este registro?");
            if (result == DialogResult.Yes)
            {
                CategoryModel deletedCategory = ((ObjectView<CategoryModel>)bsCategories.CurrencyManager.Current).Object;

                deletedCategory.State = EntityState.Deleted;

                int deletedRows = deletedCategory.SaveChanges();

                if(deletedRows < 1)
                {
                    categoryView.ShowErrorMessage("No se pudo eliminar el registro");
                    return;
                }

                bsCategories.RemoveCurrent();
                categoryView.ShowSuccessMessage("El resgistro se elimino correctamente");
            }
        }

        public void ChangeEditState()
        {
            if(categoryView.BsCategories.CurrencyManager.Position < 0)
            {
                categoryView.ShowInfoMessage("No hay registros que editar");
                return;
            }

            categoryView.SavedMode = SavedMode.Update;
            categoryView.ChangeEnebledInEditState();
            if (categoryView.ButtonEditText == Properties.Settings.Default.buttonEditText)
            {
                categoryView.ChangeEditToCancel();
            }
            else
            {
                categoryView.ChangeCancelToEdit();
            }
        }

        public void ChangeInsertState()
        {
            BindingSource bsCategories = categoryView.BsCategories;
            DataGridView dgvCategories = categoryView.DgvCategories;

            categoryView.SavedMode = SavedMode.Add;
            categoryView.ChangeEnebledInInsertState();

            if (categoryView.ButtonAddText == Properties.Settings.Default.buttonAddText)
            {
                categoryView.ChangeAddToCancel();
                bsCategories.AddNew();
            }
            else
            {
                categoryView.ChangeCancelToAdd();
                bsCategories.RemoveAt(bsCategories.Count - 1);
            }
            dgvCategories.Enabled = !dgvCategories.Enabled;
        }

        public void ChangeStateAfterSave()
        {
            if (categoryView.SavedMode == SavedMode.Add)
            {
                DataGridView dgvCategories = categoryView.DgvCategories;
                categoryView.ChangeEnebledInInsertState();
                categoryView.ChangeCancelToAdd();
                dgvCategories.Enabled = !dgvCategories.Enabled;
            }
            else
            {
                categoryView.ChangeEnebledInEditState();
                categoryView.ChangeCancelToEdit();
            }
        }
    }
}
