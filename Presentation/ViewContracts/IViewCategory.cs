﻿using Bunifu.Framework.UI;
using Equin.ApplicationFramework;
using Presentation.Helps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentation.ViewContracts
{
    interface IViewCategory
    {
        DataGridView DgvCategories { get; }
        BindingSource BsCategories { get; }
        string FilterText { get; }
        string CategoryCode { get; set; }
        string CategoryName { get; set; }
        string CategoryDescription { get; set; }
        SavedMode SavedMode { get; set; }
        void ChangeEditToCancel();
        void ChangeCancelToEdit();
        void ChangeAddToCancel();
        void ChangeCancelToAdd();
        void ChangeEnebledInEditState();
        void ChangeEnebledInInsertState();
        void ShowSuccessMessage(string message);
        void ShowErrorMessage(string message);
        void ShowInfoMessage(string message);
        DialogResult ShowQuestionMessage(string message);
        string ButtonEditText { get; }
        string ButtonAddText { get; }
    }
}
