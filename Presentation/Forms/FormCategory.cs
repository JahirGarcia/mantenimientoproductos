﻿using Bunifu.Framework.UI;
using Presentation.Forms;
using Presentation.Helps;
using Presentation.Presenters;
using Presentation.ViewContracts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentation
{
    public partial class FormCategory : Form, IViewCategory
    {
        private CategoryPresenter categoryPresenter;
        private SavedMode savedMode;
        public FormCategory()
        {
            InitializeComponent();
            categoryPresenter = new CategoryPresenter(this);
        }

        public DataGridView DgvCategories => dgvCategories;
        public BindingSource BsCategories => bsCategories;
        public string FilterText => txtFilter.Text;
        public string CategoryCode { get => txtCode.Text; set => txtCode.Text = value; }
        public string CategoryName { get => txtName.Text; set => txtName.Text = value; }
        public string CategoryDescription { get => txtDescription.Text; set => txtDescription.Text = value; }
        public SavedMode SavedMode { get => savedMode; set => savedMode = value; }
        public string ButtonEditText => btnEdit.Text;
        public string ButtonAddText => btnAdd.Text;

        public void ChangeEditToCancel()
        {
            SetButtonDangerStyle(btnEdit, Properties.Settings.Default.buttonCancelText);
        }

        public void ChangeCancelToEdit()
        {
            SetButtonPrimaryStyle(btnEdit, Properties.Settings.Default.buttonEditText);
        }

        public void ChangeAddToCancel()
        {
            SetButtonDangerStyle(btnAdd, Properties.Settings.Default.buttonCancelText);
        }

        public void ChangeCancelToAdd()
        {
            SetButtonPrimaryStyle(btnAdd, Properties.Settings.Default.buttonAddText);
        }

        public void ChangeEnebledInEditState()
        {
            txtName.Enabled = txtDescription.Enabled = btnSave.Enabled = !btnSave.Enabled;
            btnAdd.Enabled = btnDelete.Enabled = !btnAdd.Enabled;
        }

        public void ChangeEnebledInInsertState()
        {
            txtName.Enabled = txtDescription.Enabled = btnSave.Enabled = !btnSave.Enabled;
            btnEdit.Enabled = btnDelete.Enabled = !btnEdit.Enabled;
        }

        public void ShowSuccessMessage(string message)
        {
            Notification.Notify("Alerta", message, NotificationContext.Success, NotificationButton.OK);
        }

        public void ShowErrorMessage(string message)
        {
            Notification.Notify("Error", message, NotificationContext.Danger, NotificationButton.OK);
        }

        public void ShowInfoMessage(string message)
        {
            Notification.Notify("Alerta", message, NotificationContext.Info, NotificationButton.OK);
        }

        public DialogResult ShowQuestionMessage(string message)
        {
            return Notification.Notify("Advertencia", message, NotificationContext.Warning, NotificationButton.Yes_No);
        }

        private void SetButtonDangerStyle(BunifuFlatButton button, string text)
        {
            button.Text = text;
            button.BackColor = button.Normalcolor = Properties.Settings.Default.dangerColor;
            button.Activecolor = button.OnHovercolor = Properties.Settings.Default.dangerDarkColor;
        }

        private void SetButtonPrimaryStyle(BunifuFlatButton button, string text)
        {
            button.Text = text;
            button.BackColor = button.Normalcolor = Properties.Settings.Default.primaryColor;
            button.Activecolor = button.OnHovercolor = Properties.Settings.Default.primaryDarkColor;
        }

        private void BindProperties()
        {
            txtCode.DataBindings.Add("Text", bsCategories, "Code", false, DataSourceUpdateMode.OnPropertyChanged);
            txtName.DataBindings.Add("Text", bsCategories, "Name", false, DataSourceUpdateMode.OnPropertyChanged);
            txtDescription.DataBindings.Add("Text", bsCategories, "Description", false, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void FormCategory_Load(object sender, EventArgs e)
        {
            categoryPresenter.ListCategory();
            BindProperties();
        }

        private void TxtFilter_OnValueChanged(object sender, EventArgs e)
        {
            categoryPresenter.FilterTable();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            categoryPresenter.Save();
            categoryPresenter.ChangeStateAfterSave();
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            categoryPresenter.ChangeEditState();
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            categoryPresenter.ChangeInsertState();
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            categoryPresenter.Remove();
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            Notification.Notify("puto", "el que lo lea", NotificationContext.Success, NotificationButton.Yes_No);
        }
    }
}
