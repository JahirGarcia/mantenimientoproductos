﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentation.Forms
{
    public partial class Notification : Form
    {
        private NotificationContext context;
        private NotificationButton buttons;

        private DialogResult btn1Result;
        private DialogResult btn2Result;

        private DialogResult result;

        public DialogResult Result { get => result; set => result = value; }

        private Notification(string title, string content, NotificationContext context, NotificationButton buttons)
        {
            InitializeComponent();
            this.title.Text = title;
            this.content.Text = content;
            this.context = context;
            this.buttons = buttons;
        }

        public static DialogResult Notify(string title, string content, NotificationContext context, NotificationButton buttons)
        {
            Notification fn = new Notification(title, content, context, buttons);
            fn.ShowDialog();
            return fn.Result;
        }

        private void FormNotification_Load(object sender, EventArgs e)
        {
            SetContextColor();
            SetButtonLayout();
            formFadeTransition.ShowAsyc(this);
        }

        private void SetContextColor()
        {
            Color color = new Color(), darkColor = new Color();
            Image image = null;

            switch(context)
            {
                case NotificationContext.Danger:
                    color = Properties.Settings.Default.dangerColor;
                    darkColor = Properties.Settings.Default.dangerDarkColor;
                    image = Properties.Resources.icons8_informe_del_sistema_100;
                    break;
                case NotificationContext.Success:
                    color = Properties.Settings.Default.successColor;
                    darkColor = Properties.Settings.Default.successDarkColor;
                    image = Properties.Resources.icons8_información_del_sistema_100;
                    break;
                case NotificationContext.Warning:
                    color = Properties.Settings.Default.warningColor;
                    darkColor = Properties.Settings.Default.warningDarkColor;
                    image = Properties.Resources.icons8_informe_del_sistema_100;
                    break;
                case NotificationContext.Info:
                    color = Properties.Settings.Default.infoColor;
                    darkColor = Properties.Settings.Default.infoDarkColor;
                    image = Properties.Resources.icons8_computer_chat_100;
                    break;
            }

            this.BackColor = color;
            btn1.BackColor = btn1.Normalcolor = btn2.BackColor = btn2.Normalcolor = color;
            btn1.Activecolor = btn1.OnHovercolor = btn2.Activecolor = btn2.OnHovercolor = darkColor;
            contextImage.Image = image;
        }

        private void SetButtonLayout()
        {
            string btn1Text = "", btn2Text = "";

            switch(buttons)
            {
                case NotificationButton.OK:
                    btn1Result = DialogResult.OK;
                    btn1Text = "OK";
                    btn2.Visible = false;
                    break;
                case NotificationButton.OK_Cancel:
                    btn1Result = DialogResult.OK;
                    btn2Result = DialogResult.Cancel;
                    btn1Text = "OK";
                    btn2Text = "Cancel";
                    break;
                case NotificationButton.Yes_No:
                    btn1Result = DialogResult.Yes;
                    btn2Result = DialogResult.No;
                    btn1Text = "Yes";
                    btn2Text = "No";
                    break;
            }

            btn1.Text = btn1Text;
            btn2.Text = btn2Text;
        }

        private void Btn1_Click(object sender, EventArgs e)
        {
            Result = btn1Result;
            Close();
        }

        private void Btn2_Click(object sender, EventArgs e)
        {
            Result = btn2Result;
            Close();
        }
    }

    public enum NotificationContext
    {
        Success,
        Danger,
        Warning,
        Info
    }

    public enum NotificationButton
    {
        OK,
        OK_Cancel,
        Yes_No
    }
}
