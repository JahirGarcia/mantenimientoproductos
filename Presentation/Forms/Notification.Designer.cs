﻿namespace Presentation.Forms
{
    partial class Notification
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.formElipse = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.title = new System.Windows.Forms.Label();
            this.content = new System.Windows.Forms.Label();
            this.dragControlContextPanel = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.contextPanel = new System.Windows.Forms.Panel();
            this.contextImage = new System.Windows.Forms.PictureBox();
            this.contentPanel = new System.Windows.Forms.Panel();
            this.btn1 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btn2 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.formFadeTransition = new Bunifu.Framework.UI.BunifuFormFadeTransition(this.components);
            this.contextPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.contextImage)).BeginInit();
            this.contentPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // formElipse
            // 
            this.formElipse.ElipseRadius = 5;
            this.formElipse.TargetControl = this;
            // 
            // title
            // 
            this.title.Dock = System.Windows.Forms.DockStyle.Top;
            this.title.Font = global::Presentation.Properties.Settings.Default.buttonFont;
            this.title.Location = new System.Drawing.Point(0, 0);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(254, 48);
            this.title.TabIndex = 1;
            this.title.Text = "Title";
            this.title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // content
            // 
            this.content.Dock = System.Windows.Forms.DockStyle.Top;
            this.content.Font = global::Presentation.Properties.Settings.Default.bodyFont;
            this.content.Location = new System.Drawing.Point(0, 48);
            this.content.Name = "content";
            this.content.Size = new System.Drawing.Size(254, 89);
            this.content.TabIndex = 2;
            this.content.Text = "Content";
            this.content.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dragControlContextPanel
            // 
            this.dragControlContextPanel.Fixed = true;
            this.dragControlContextPanel.Horizontal = true;
            this.dragControlContextPanel.TargetControl = this.contextPanel;
            this.dragControlContextPanel.Vertical = true;
            // 
            // contextPanel
            // 
            this.contextPanel.BackColor = System.Drawing.Color.Transparent;
            this.contextPanel.Controls.Add(this.contextImage);
            this.contextPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.contextPanel.Location = new System.Drawing.Point(0, 0);
            this.contextPanel.Name = "contextPanel";
            this.contextPanel.Size = new System.Drawing.Size(259, 145);
            this.contextPanel.TabIndex = 2;
            // 
            // contextImage
            // 
            this.contextImage.Image = global::Presentation.Properties.Resources.icons8_información_del_sistema_100;
            this.contextImage.Location = new System.Drawing.Point(77, 27);
            this.contextImage.Name = "contextImage";
            this.contextImage.Size = new System.Drawing.Size(99, 91);
            this.contextImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.contextImage.TabIndex = 0;
            this.contextImage.TabStop = false;
            // 
            // contentPanel
            // 
            this.contentPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.contentPanel.BackColor = global::Presentation.Properties.Settings.Default.backColor;
            this.contentPanel.Controls.Add(this.btn1);
            this.contentPanel.Controls.Add(this.btn2);
            this.contentPanel.Controls.Add(this.content);
            this.contentPanel.Controls.Add(this.title);
            this.contentPanel.Location = new System.Drawing.Point(2, 146);
            this.contentPanel.Name = "contentPanel";
            this.contentPanel.Size = new System.Drawing.Size(254, 201);
            this.contentPanel.TabIndex = 1;
            // 
            // btn1
            // 
            this.btn1.Activecolor = global::Presentation.Properties.Settings.Default.successDarkColor;
            this.btn1.BackColor = global::Presentation.Properties.Settings.Default.successColor;
            this.btn1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn1.BorderRadius = 5;
            this.btn1.ButtonText = "OK";
            this.btn1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn1.DisabledColor = System.Drawing.Color.Gray;
            this.btn1.Iconcolor = System.Drawing.Color.Transparent;
            this.btn1.Iconimage = null;
            this.btn1.Iconimage_right = null;
            this.btn1.Iconimage_right_Selected = null;
            this.btn1.Iconimage_Selected = null;
            this.btn1.IconMarginLeft = 0;
            this.btn1.IconMarginRight = 0;
            this.btn1.IconRightVisible = true;
            this.btn1.IconRightZoom = 0D;
            this.btn1.IconVisible = true;
            this.btn1.IconZoom = 90D;
            this.btn1.IsTab = false;
            this.btn1.Location = new System.Drawing.Point(11, 151);
            this.btn1.Name = "btn1";
            this.btn1.Normalcolor = global::Presentation.Properties.Settings.Default.successColor;
            this.btn1.OnHovercolor = global::Presentation.Properties.Settings.Default.successDarkColor;
            this.btn1.OnHoverTextColor = System.Drawing.Color.White;
            this.btn1.selected = false;
            this.btn1.Size = new System.Drawing.Size(112, 40);
            this.btn1.TabIndex = 4;
            this.btn1.Text = "OK";
            this.btn1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn1.Textcolor = System.Drawing.Color.White;
            this.btn1.TextFont = global::Presentation.Properties.Settings.Default.buttonFont;
            this.btn1.Click += new System.EventHandler(this.Btn1_Click);
            // 
            // btn2
            // 
            this.btn2.Activecolor = global::Presentation.Properties.Settings.Default.successDarkColor;
            this.btn2.BackColor = global::Presentation.Properties.Settings.Default.successColor;
            this.btn2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn2.BorderRadius = 5;
            this.btn2.ButtonText = "Cancel";
            this.btn2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2.DisabledColor = System.Drawing.Color.Gray;
            this.btn2.Iconcolor = System.Drawing.Color.Transparent;
            this.btn2.Iconimage = null;
            this.btn2.Iconimage_right = null;
            this.btn2.Iconimage_right_Selected = null;
            this.btn2.Iconimage_Selected = null;
            this.btn2.IconMarginLeft = 0;
            this.btn2.IconMarginRight = 0;
            this.btn2.IconRightVisible = true;
            this.btn2.IconRightZoom = 0D;
            this.btn2.IconVisible = true;
            this.btn2.IconZoom = 90D;
            this.btn2.IsTab = false;
            this.btn2.Location = new System.Drawing.Point(132, 150);
            this.btn2.Name = "btn2";
            this.btn2.Normalcolor = global::Presentation.Properties.Settings.Default.successColor;
            this.btn2.OnHovercolor = global::Presentation.Properties.Settings.Default.successDarkColor;
            this.btn2.OnHoverTextColor = System.Drawing.Color.White;
            this.btn2.selected = false;
            this.btn2.Size = new System.Drawing.Size(112, 40);
            this.btn2.TabIndex = 3;
            this.btn2.Text = "Cancel";
            this.btn2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn2.Textcolor = System.Drawing.Color.White;
            this.btn2.TextFont = global::Presentation.Properties.Settings.Default.buttonFont;
            this.btn2.Click += new System.EventHandler(this.Btn2_Click);
            // 
            // formFadeTransition
            // 
            this.formFadeTransition.Delay = 1;
            // 
            // Notification
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = global::Presentation.Properties.Settings.Default.successColor;
            this.ClientSize = new System.Drawing.Size(259, 350);
            this.Controls.Add(this.contextPanel);
            this.Controls.Add(this.contentPanel);
            this.ForeColor = global::Presentation.Properties.Settings.Default.bodyColor;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Notification";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Notification";
            this.Load += new System.EventHandler(this.FormNotification_Load);
            this.contextPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.contextImage)).EndInit();
            this.contentPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse formElipse;
        private System.Windows.Forms.Label content;
        private System.Windows.Forms.Label title;
        private Bunifu.Framework.UI.BunifuDragControl dragControlContextPanel;
        private System.Windows.Forms.Panel contentPanel;
        private System.Windows.Forms.Panel contextPanel;
        private Bunifu.Framework.UI.BunifuFlatButton btn1;
        private Bunifu.Framework.UI.BunifuFlatButton btn2;
        private Bunifu.Framework.UI.BunifuFormFadeTransition formFadeTransition;
        private System.Windows.Forms.PictureBox contextImage;
    }
}